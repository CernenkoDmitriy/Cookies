package classWork.filters;

import classWork.filters.filter.LoggerFilter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import classWork.filters.Entities.MyLogger;


@WebServlet("/table/logger")
public class LoggerServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<MyLogger> log = (List<MyLogger>) req.getServletContext().getAttribute("logger");
        if (log!=null) {
            for (MyLogger l : log) {
                req.setAttribute("log", log);
            }
        }


        req.getRequestDispatcher("/WEB-INF/view/classWork/logger.jsp").forward(req, resp);
    }


}
