package classWork.filters;

import classWork.filters.Entities.User;
import classWork.filters.database.databaseClass;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/table")
public class TableServlet extends HttpServlet {
    private Connection connection;

    @Override
    public void init() throws ServletException {
        connection = databaseClass.connectDB();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ResultSet usersSet;
        List<User> users = new ArrayList<>();

        if (connection != null) {
            try {
                usersSet = databaseClass.SelectAll(connection.createStatement());
                while (usersSet.next()) {
                    users.add(new User(usersSet.getString("id"),
                            usersSet.getString("login"),
                            usersSet.getString("password"),
                            usersSet.getString("role")));
                }
                req.setAttribute("users", users);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        req.getRequestDispatcher("/WEB-INF/view/classWork/table.jsp").forward(req, resp);
    }
}
