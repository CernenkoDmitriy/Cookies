package classWork.filters;

import classWork.filters.database.databaseClass;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/authorizationClass")
public class LoginServlet extends HttpServlet {
    private Connection connection;

    @Override
    public void init() throws ServletException {
        connection = databaseClass.connectDB();
        try {
            assert connection != null;
            databaseClass.initDatabase(connection.createStatement());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/view/classWork/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        if (connection != null) {
            try {
                ResultSet user = databaseClass.CheckUser(login, password, connection.createStatement());
                if (user.next()) {
                    Cookie cookie = new Cookie("Authentication", "true");
                    cookie.setHttpOnly(true);
                    cookie.setPath("/");
                    resp.addCookie(cookie);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        resp.sendRedirect("/table");
    }
}
