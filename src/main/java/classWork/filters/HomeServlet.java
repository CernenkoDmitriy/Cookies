package classWork.filters;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/homeclass")
public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/view/classWork/home.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();

         for (Cookie c : cookies) {
            if (c.getName().equals("Authentication")) {
                Cookie cookie = new Cookie("Authentication", "false");
                cookie.setMaxAge(0);
                resp.addCookie(cookie);
                break;
            }
        }
        req.getRequestDispatcher("/WEB-INF/view/classWork/home.jsp").forward(req, resp);

    }
}
