package classWork.filters.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//@WebFilter("/table")
public class AuthentictionFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        Cookie[] cookies = req.getCookies();
        boolean isAout = false;

        for (Cookie c : cookies) {
            if (c.getName().equals("Authentication")) {
                isAout = true;
                break;
            }
        }
        if (isAout) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            resp.sendRedirect("/homeclass");
        }

    }

    @Override
    public void destroy() {

    }
}
