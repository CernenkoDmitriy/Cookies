package classWork.filters.filter;

import classWork.filters.Entities.MyLogger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//@WebFilter( urlPatterns ="/*")
public class LoggerFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        List<MyLogger> log = (List<MyLogger>) servletRequest.getServletContext().getAttribute("logger");
        if (log==null){
            log = new ArrayList<>();
        }
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        SimpleDateFormat formatForDateNow = new SimpleDateFormat("E yyyy.MM.dd hh:mm:ss");
        log.add(new MyLogger(formatForDateNow.format(new Date()), req.getMethod(), req.getRequestURI()));
        servletRequest.getServletContext().setAttribute("logger", log);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
