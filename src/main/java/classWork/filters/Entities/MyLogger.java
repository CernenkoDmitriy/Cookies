package classWork.filters.Entities;

public class MyLogger {
    private String time;
    private String url;
    private String method;

    public MyLogger(String time, String url, String method) {
        this.time = time;
        this.url = url;
        this.method = method;
    }

    public String getTime() {
        return time;
    }

    public String getUrl() {
        return url;
    }

    public String getMethod() {
        return method;
    }
}
