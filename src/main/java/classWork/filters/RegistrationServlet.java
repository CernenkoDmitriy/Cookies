package classWork.filters;

import classWork.filters.database.databaseClass;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/registrationClass")
public class RegistrationServlet extends HttpServlet {
    private Connection connection;

    @Override
    public void init() throws ServletException {
        connection = databaseClass.connectDB();
        try {
            assert connection != null;
            databaseClass.initDatabase(connection.createStatement());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/view/classWork/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String role = req.getParameter("role");

        if (!login.isEmpty() && !password.isEmpty() && !role.isEmpty()){
            try {
                if (connection != null && !connection.isClosed()) {
                    Statement stmt = connection.createStatement();
                    databaseClass.insertPerson(login, password, role, stmt);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            resp.sendRedirect("/authorizationClass");
        }else {
            resp.sendRedirect("/registrationClass");
        }
    }
}
