package classWork.filters.database;

import java.sql.*;

public class databaseClass {
    public static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/site_filter" +
            "?characterEncoding=UTF-8&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC& useSSL=true";
    public static final String USER = "root";
    public static final String PASS = "ghbvf";

    public static Connection connectDB() {
        try {
            return DriverManager.getConnection(CONNECTION_STRING, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void initDatabase(Statement statement) {
        String sql = "CREATE TABLE IF NOT EXISTS user(" +
                "   id INT PRIMARY KEY AUTO_INCREMENT," +
                "   login VARCHAR(255) NOT NULL UNIQUE," +
                "   password VARCHAR(255) NOT NULL," +
                "   role VARCHAR(255) NOT NULL);";
        try {
            statement.execute(sql);
        } catch (SQLException e) {           
            
        }
    }

    public static void insertPerson(String login, String password, String role, Statement statement) {
        String sql = String.format("INSERT INTO site_filter.user (login, password, role) " +
                "VALUES ('%s', '%s', '%s');",  login, password, role);
        try {
            statement.execute(sql);
        } catch (SQLException e) {

        }
    }

    public static ResultSet SelectAll(Statement stmt){
        String sql = "SELECT * FROM user";
        try {
            return stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public  static ResultSet CheckUser(String login, String password, Statement stmt){
        String sql =String.format("SELECT * FROM site_filter.user  " +
                "WHERE login='%s' AND `password`='%s';", login, password);
        try {
            return stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
