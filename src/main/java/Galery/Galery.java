package Galery;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/galery")
@MultipartConfig(location = "C:\\Cookies\\src\\main\\webapp\\resources\\images")
public class Galery extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Path> files = Files.list(Paths.get("C:\\Cookies\\src\\main\\webapp\\resources\\images\\"))
                .collect(Collectors.toList());
        req.setAttribute("galery", files);
        req.getRequestDispatcher("/WEB-INF/view/Galery.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Collection<Part> part = req.getParts();
        if (part != null) {
            for (Part p : part) {
                if (p.getSize()>0) {
                    p.write(getFileName(p));
                }
            }
        }
        doGet(req, resp);
    }

    private String getFileName(Part part) {
        String fileName = null;
        String contentDisposition = part.getHeader("Content-Disposition");
        String[] data = contentDisposition.split(";");

        for (String str : data) {
            String s = str.trim();
            if (s.startsWith("filename")) {
                int idx = s.indexOf("=");
                fileName = s.substring(idx + 2, s.length() - 1);
            }
        }
        return fileName;
    }

}
