package database;

public class SQLCommands {
    public static String CREATE_TABLE_PERSON = "CREATE TABLE IF NOT EXISTS person(\n" +
            "   id INT PRIMARY KEY AUTO_INCREMENT,\n" +
            "   firstName VARCHAR(255) NOT NULL, \n" +
            "   lastName VARCHAR(255) NOT NULL, \n" +
            "   login VARCHAR(255) NOT NULL UNIQUE,\n" +
            "   password VARCHAR(255) NOT NULL,\n" +
            "   CONSTRAINT login_password_unique UNIQUE (login, password))";
    public static final String DROP_PERSON = "DROP TABLE IF EXISTS person\n";


}
