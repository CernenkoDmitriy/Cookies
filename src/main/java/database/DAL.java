package database;

import java.sql.*;

public class DAL {
    public static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/mySite" +
            "?characterEncoding=UTF-8&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC& useSSL=true";
    public static final String USER = "root";
    public static final String PASS = "ghbvf";

    public static Connection connectDB() {
        try {
            return DriverManager.getConnection(CONNECTION_STRING, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void initDatabase(Statement stmt) {
        try {
            stmt.execute(SQLCommands.CREATE_TABLE_PERSON);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void dropDatabase(Statement stmt) {
        try {
            stmt.execute(SQLCommands.DROP_PERSON);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertPerson(String firstName, String lastName, String login, String password, Statement stmt) {
        String sql = String.format("INSERT INTO mySite.person (firstName, lastName, login, password) " +
                "VALUES ('%s', '%s', '%s', '%s');", firstName, lastName, login, password);
        try {
            stmt.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
