<%@ page import="java.sql.ResultSet" %>
<%@ page import="classWork.filters.Entities.User" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="/resources/css/css.css"/>
</head>
<body>
<div class="navbar navbar-nav bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/homeclass">Main page</a>
        <form method="post" action="/homeclass">
            <input type="submit" class="btn bg-white" value="Sign out">
        </form>
    </div>
</div>
<p>Click here to see <a href="/table/logger" style="color: #fd7e14;">Logger</a></p>
<div class="container">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Cookies name</th>
            <th>Cookies id</th>
        </tr>
        </thead>
        <tbody>
        <% Cookie[] cookies = request.getCookies();%>
        <% for (Cookie c : cookies) {%>
        <tr>
            <td><%= c.getName()%>
            </td>
            <td><%= c.getValue()%>
            </td>
        </tr>
        <% }%>
        </tbody>
    </table>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>User name</th>
            <th>Pass</th>
            <th>Role</th>
        </tr>
        </thead>
        <tbody>
        <% for (User c : (List<User>) request.getAttribute("users")) {%>
        <tr>
            <td><%= c.getLogin()%>
            </td>
            <td><%= c.getPassword()%>
            </td>
            <td><%= c.getRole()%>
            </td>
        </tr>
        <% }%>
        </tbody>
    </table>
</div>
</body>
</html>
