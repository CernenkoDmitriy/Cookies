<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="/resources/css/css.css"/>
    <source>
</head>
<body>
<div class="navbar navbar-nav bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/homeclass">Main page</a>
        <form method="post" action="/homeclass">
            <input type="submit" class="btn bg-white" value="Sign out">
        </form>
    </div>
</div>
<div class="wrapper ">
    <form method="post" action='/registrationClass' class="form-horizontal">
        <h2>Регистрация</h2>
        <div class="row">
            <div class="col-md-6">
                <input type="text" name="login" class="form-control" placeholder="Login">
            </div>
            <div class="col-md-6">
                <input type="text" name="password" class="form-control" placeholder="Password">
            </div>
        </div><br>
        <select class="form-control" name="role">
            <option disabled selected>-Choose role-</option>
            <option value="admin">Admin</option>
            <option value="guest">Guest</option>
            <option value="user">User</option>
        </select><br>
        <input type="submit" value="Регистрация" class="btn btn-secondary">
    </form>
</div>
</body>
<script src="/resources/js/js.js"></script>
</html>
