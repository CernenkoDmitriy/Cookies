<%@ page import="java.sql.ResultSet" %>
<%@ page import="classWork.filters.Entities.MyLogger" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: root
  Date: 22.06.2018
  Time: 16:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Logger</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="/resources/css/css.css"/>
</head>
<body>
<div class="navbar navbar-nav bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/homeclass">Main page</a>
        <form method="post" action="/homeclass">
            <input type="submit" class="btn bg-white" value="Sign out">
        </form>
    </div>
</div>
<div class="container">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Method</th>
            <th>URL</th>
            <th>Time</th>
        </tr>
        </thead>
        <tbody>
        <% List<MyLogger> log = (List<MyLogger>) request.getAttribute("log"); %>
        <% if (log != null) {
            for (MyLogger logger : log) {%>
        <tr>
            <td><%= logger.getMethod()%>
            </td>
            <td><%= logger.getUrl()%>
            </td>
            <td><%= logger.getTime()%>
            </td>
        </tr>
        <%
                }
            }
        %>
        </tbody>
    </table>
</div>
</body>
</html>
