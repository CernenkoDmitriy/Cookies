<%--
  Created by IntelliJ IDEA.
  User: student
  Date: 20.06.2018
  Time: 20:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="/resources/css/css.css"/>
</head>
<body>
<div class="navbar navbar-nav bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/homeclass">Main page</a>
        <form method="post" action="/homeclass">
            <input type="submit" class="btn bg-white" value="Sign out">
        </form>
    </div>
</div>
<div class="wrapper ">
    <form method="post" action='/authorizationClass' class="form-horizontal">
        <h2>Авторизация</h2>
        <div class="row">
            <div class="col-md-6">
                <input type="text" name="login" class="form-control" placeholder="Login">
            </div>
            <div class="col-md-6">
                <input type="text" name="password" class="form-control" placeholder="Password">
            </div>
        </div><br>

        <input type="submit" value="Авторизация" class="btn btn-secondary">
    </form>
</div>
</body>
</html>
