<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="/resources/css/css.css"/>
</head>
<body>
<div class="navbar navbar-nav bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/homeclass">Main page</a>
        <form method="post" action="/homeclass" class="nav-item">
            <input type="submit" class="btn bg-white" value="Sign out">
        </form>
    </div>
</div>
<div class="wrapper">
    <p>Page <a href="/table" class="btn btn-secondary">Table</a> </p>
    <div class="row">
        <h1>Пройдите по ссылке для регистрации или авторизации</h1><br>
        <div class="col-md-6"><a href="/registrationClass" class="btn btn-secondary">Registration (Class work)</a></div>
        <div class="col-md-6"><a href="/authorizationClass" class="btn btn-secondary">Authorization</a></div>
    </div>
</div>
</body>
</html>
