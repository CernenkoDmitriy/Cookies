<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="/resources/css/css.css"/>
    <source>
</head>
<body>
<div class="wrapper ">
    <form method="post" action='/login' class="form-horizontal">
        <h2>Регистрация</h2>
        <p>Это беспдатно и всегда будет бесплатно.</p>
        <div class="row">
            <div class="col-md-6">
                <input type="text" name="firstName" class="form-control" placeholder="Имя">
            </div>
            <div class="col-md-6">
                <input type="text" name="lastName" class="form-control" placeholder="Фамилия">
            </div>
        </div>
        <input type="text" name="login" class="form-control" placeholder="Эл. фдрес или номер моб телефона">
        <input type="password" name="password" class="form-control" placeholder="Пароль">
        <input type="password" name="checkPassword" class="form-control" placeholder="Введите пароль повторно">
        <p>День рождения</p>
        <input type="date" name="dateBirthday" class="form-control">
        <label><input type="radio" name="sex" value="male"><span class="mg-5">Женский</span></label>
        <label><input type="radio" name="sex" value="female"><span class="mg-5">Мужской</span></label><br>
        <input type="submit" value="Регистрация" class="btn btn-secondary">
    </form>
</div>
</body>
<script src="/resources/js/js.js"></script>
</html>
