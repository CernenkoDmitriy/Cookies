<%@ page import="java.nio.file.Path" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: student
  Date: 04.07.2018
  Time: 20:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Galery</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="/resources/css/css.css"/>
</head>
<body>
    <div class="container">
        <form action="/galery" method="post" enctype="multipart/form-data">
            <input type="file" name="file" multiple>
            <button type="submit">Send</button>
        </form>
    </div>



<% List<Path> files = (List<Path>) request.getAttribute("galery");%>
    <div class="container">
<% if(files!=null){ for (Path path:files) {%>
    <img src="/resources/images/<%= path.getFileName()%>" style="max-width: 200px; max-height: 200px; float: left; margin: 15px; border-radius: 50px;">
<%}}%>
    </div>

</body>
</html>
