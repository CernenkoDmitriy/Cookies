<%@page pageEncoding="utf-8" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="/resources/css/css.css"/>
</head>
<body>

<div class="navbar navbar-nav bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/homeclass">Main page</a>
        <form method="post" action="/homeclass">
            <input type="submit" class="btn bg-white" value="Sign out">
        </form>
    </div>
</div>
<div class="wrapper">
    <div class="row">
        <h1>Пройдите по ссылке для регистрации или авторизации</h1><br>
        <div class="col-md-4"><a href="/registrationClass" class="btn btn-secondary">Registration (Class work)</a></div>
        <div class="col-md-4"><a href="/authorizationClass" class="btn btn-secondary">Authorization</a></div>
        <div class="col-md-4"><a href="/galery" class="btn btn-secondary">Galery</a></div>
    </div>
</div>

</body>
</html>
